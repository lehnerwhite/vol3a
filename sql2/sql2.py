import sqlite3 as sql
import csv
import pandas as pd

def prob1():
    """
    Specify relationships between columns in given sql tables.
    """
    print "One-to-one relationships:"
    print "Student ID -> Name"
    print "Course ID -> Course Name"
    print "Major ID -> Major Name"
    # Put print statements specifying one-to-one relationships between table
    # columns.

    print "**************************"
    print "One-to-many relationships:"
    print "Major ID/Major Name -> Student ID/Student Name"
    print "Minor ID/Minor Name -> Student ID/Student Name"
    # Put print statements specifying one-to-many relationships between table
    # columns.

    print "***************************"
    print "Many-to-Many relationships:"
    print "Course ID -> Student ID"
    print "Course Grade -> Student ID"
    # Put print statements specifying many-to-many relationships between table
    # columns.

def prob2():
    """
    Write a SQL query that will output how many students belong to each major,
    including students who don't have a major.

    Return: A table indicating how many students belong to each major.
    """
    # Connecting the database and creating the cursor
    db = sql.connect("sql")
    cur = db.cursor()
    
    # Create the table in the database from the csv files
    cur.execute("DROP TABLE IF EXISTS classes;")
    cur.execute("CREATE TABLE classes (id INT NOT NULL, name TEXT);")
    with open('classes.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
        cur.executemany("INSERT INTO classes VALUES (?, ?);", rows)

    cur.execute("DROP TABLE IF EXISTS grades;")
    cur.execute("CREATE TABLE grades (id INT NOT NULL, class_id INT, grade TEXT);")
    with open('grades.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
        cur.executemany("INSERT INTO grades VALUES (?, ?, ?);", rows)

    cur.execute("DROP TABLE IF EXISTS fields;")
    cur.execute("CREATE TABLE fields (id INT NOT NULL, name TEXT);")
    with open('fields.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
        cur.executemany("INSERT INTO fields VALUES (?, ?);", rows)
    
    cur.execute("DROP TABLE IF EXISTS students;")
    cur.execute("CREATE TABLE students (id INT NOT NULL, name TEXT, major INT, minor INT);")
    with open('students.csv', 'rb') as csvfile:
        rows = [row for row in csv.reader(csvfile, delimiter=',')]
        cur.executemany("INSERT INTO students VALUES (?, ?, ?, ?);", rows)
    
    query = "SELECT fields.name, COUNT(students.id) FROM students LEFT OUTER JOIN fields ON students.major=fields.id GROUP BY fields.name ORDER BY fields.name;"

    # This line will make a pretty table with the results of your query.
        ### query is a string containing your sql query
        ### db is a sql database connection
    result =  pd.read_sql_query(query, db)
    db.commit()
    db.close()

    return result

def prob3():
    """
    Select students who received two or more non-Null grades in their classes.

    Return: A table of the students' names and the grades each received.
    """
    #Build your tables and/or query here
    db = sql.connect("sql")
    cur = db.cursor()
    
    query = "SELECT students.name, COUNT(grades.grade) FROM students LEFT OUTER JOIN grades ON students.id=grades.id GROUP BY students.name HAVING COUNT(grades.grade) >= 2;"

    # This line will make a pretty table with the results of your query.
        ### query is a string containing your sql query
        ### db is a sql database connection
    result =  pd.read_sql_query(query, db)
    db.commit()
    db.close()

    return result

def prob4():
    """
    Get the average GPA at the school using the given tables.

    Return: A float representing the average GPA, rounded to 2 decimal places.
    """
    db = sql.connect("sql")
    cur = db.cursor()
    cur.execute("SELECT ROUND(AVG(points),2) FROM (SELECT CASE WHEN grade IN ('A+','A','A-') THEN 4.0 WHEN grade IN ('B+','B','B-') THEN 3.0 WHEN grade IN ('C+','C','C-') THEN 2.0 WHEN grade IN ('D+','D','D-') THEN 1.0 END AS points FROM grades WHERE grade IS NOT NULL);")
    result = cur.fetchone()[0]
    db.commit()
    db.close()

    return result

def prob5():
    """
    Find all students whose last name begins with 'C' and their majors.

    Return: A table containing the names of the students and their majors.
    """
    #Build your tables and/or query here
    db = sql.connect("sql")
    cur = db.cursor()
    query  = "SELECT students.name, fields.name FROM students LEFT OUTER JOIN fields ON students.major=fields.id WHERE students.name LIKE '% C%';"


    # This line will make a pretty table with the results of your query.
        ### query is a string containing your sql query
        ### db is a sql database connection
    result =  pd.read_sql_query(query, db)

    db.commit()
    db.close()

    return result
