import re

def prob1():
  pattern_string = r"\^\{\(!%\.\*_\)\}&" # Edit this line
  pattern = re.compile(pattern_string)
  print bool(pattern.match("^{(!%.*_)}&"))

def prob2():
    pattern_string = r"^Book store$|^Book supplier$|^Mattress store$|^Mattress supplier$|^Grocery store$|^Grocery supplier$" # Edit this line
    pattern = re.compile(pattern_string)
    strings_to_match = [ "Book store", "Book supplier", "Mattress store", \
    "Mattress supplier", "Grocery store", "Grocery supplier"]
    not_to_match = ["Book store sale", "Grocery Book store"]
    print all(pattern.match(string) for string in strings_to_match) and not all(pattern.match(string) for string in not_to_match)

def prob3():
    pattern_string = r"^[^d-w]$" # Edit this line
    pattern = re.compile(pattern_string)
    strings_to_match = ["a", "b", "c", "x", "y", "z"]
    uses_line_anchors = (pattern_string.startswith('^') and pattern_string.endswith('$'))
    solution_is_clever = (len(pattern_string) == 8)
    matches_list = all(pattern.match(string) for string in strings_to_match)
    print uses_line_anchors and solution_is_clever and matches_list

def prob4():
    identifier_pattern_string = r"^\D\w\w\w\w$" # Edit this line
    identifier_pattern = re.compile(identifier_pattern_string)
    valid = ["mouse", "HORSE", "_1234", "__x__", "while"]
    not_valid = ["3rats", "err*r", "sq(x)", "too_long"]
    print all(identifier_pattern.match(string) for string in valid) and not any(identifier_pattern.match(string) for string in not_valid)

def prob5():
    identifier_pattern_string = r"^\D\w{1,}$" # Edit this line
    identifier_pattern = re.compile(identifier_pattern_string)
    valid = ["mouse", "HORSE", "_1234", "__x__", "while","Longer_String_Than_Before"]
    not_valid = ["3rats", "err*r", "sq(x)"]
    print all(identifier_pattern.match(string) for string in valid) and not any(identifier_pattern.match(string) for string in not_valid)

def prob6():
    filename = 'contacts.txt'
    d = dict()
    email_string = r"^\w+@\w+.\w+$"
    number_string = r"^\(?\d{3}\)?\-?\d\d\d-\d\d\d\d$"
    bday_string = r"^\d+/\d+/\d{2,4}$"
    email = re.compile(email_string)
    number = re.compile(number_string)
    bday = re.compile(bday_string)
    with open(filename, 'r') as f:
        for line in f:
            line = line.split()
            name = "{} {}".format(line[0], line[1])
            d[name] = {}
            for i in xrange(2,len(line)):
                if email.match(line[i]):
                    d[name]["email"] = line[i]
                elif number.match(line[i]):
                    d[name]["phone"] = line[i]
                elif bday.match(line[i]):
                    d[name]["bday"] = line[i]
    return d 

def prob7():
    pass
