import pickle
import numpy as np
import pandas as pd
from pyproj import Proj, transform
from bokeh.plotting import Figure
from bokeh.models import WMTSTileSource, ColumnDataSource, HoverTool, Select
from bokeh.plotting import figure, output_file, show
from bokeh.io import output_file, show, curdoc
from bokeh.models.widgets import Select
from bokeh.layouts import column
from bokeh.palettes import Greens9, Reds9

with open("fars_data/Pickle/id_to_state.pickle") as file:
    id_to_state = pickle.load(file)
    
with open("fars_data/Pickle/us_states.pickle") as file:
    us_states = pickle.load(file)
    
a_cols = ['ST_CASE', 'STATE', 'LATITUDE', 'LONGITUD', 'FATALS', 'HOUR', 'DAY', 'MONTH', 'YEAR', 'DRUNK_DR']
v_cols = ['ST_CASE','VEH_NO', 'SPEEDREL']
p_cols = ['ST_CASE', 'PER_TYP', 'DRINKING']

adfs = []
pdfs = []
vdfs = []

for yr in ['10','11','12','13','14']:
    with open("./fars_data/fars_data/Accidents/accident{}.pickle".format(yr)) as my_file:
        adf = pd.DataFrame(pickle.load(my_file))
        adfs.append(adf)
    with open("./fars_data/fars_data/Person/person{}.pickle".format(yr)) as my_file:
        pdf = pd.DataFrame(pickle.load(my_file))
        pdfs.append(pdf)
    with open("./fars_data/fars_data/Vehicle/vehicle{}.pickle".format(yr)) as my_file:
        vdf = pd.DataFrame(pickle.load(my_file))
        vdf['YEAR'] = int('20{}'.format(yr))
        vdfs.append(vdf)

accidents = pd.DataFrame()

for i in xrange(5):
    adf, vdf = adfs[i].loc[:,a_cols], vdfs[i].loc[:,['ST_CASE', 'SPEEDREL']]
    adf["LONGITUD"] = adf["LONGITUD"].replace([777.7777, 888.8888, 999.9999], np.nan)
    adf["LATITUDE"] = adf["LATITUDE"].replace([77.7777, 88.8888, 99.9999], np.nan)
    adf.dropna(inplace=True)    
    
    vdf['SPEEDREL'] = np.where((vdf['SPEEDREL'] >= 8), 0, vdf['SPEEDREL'] )
    together = pd.merge(adf, vdf, on='ST_CASE')
    
    adf['SPEEDING'] = together.groupby('ST_CASE').sum()['SPEEDREL'].values
    adf['SPEEDING'] = (adf['SPEEDING'] != 0).astype(int)
    
    adf['STATE'] = [id_to_state[i] for i in adf['STATE'].replace(0,49)]
    
    accidents = pd.concat([accidents, adf])

from_proj = Proj(init="epsg:4326")
to_proj = Proj(init="epsg:3857")

def convert(longitudes, latitudes):
    """Converts latlon coordinates to meters.
    Inputs:
    longitudes (array-like) : array of longitudes
    latitudes (array-like) : array of latitudes
    Example:
    x,y = convert(accidents.LONGITUD, accidents.LATITUDE)
    """
    x_vals = []
    y_vals = []
    for lon, lat in zip(longitudes, latitudes):
        x, y = transform(from_proj, to_proj, lon, lat)
        x_vals.append(x)
        y_vals.append(y)
    return x_vals, y_vals
accidents["x"], accidents["y"] = convert(accidents.LONGITUD, accidents.LATITUDE)

##################################################################

output_file("bokeh_prob10.html")

fig = Figure(plot_width=1100, plot_height=650, x_range=(-13000000, -7000000), 
             y_range=(2750000, 6250000),tools=["wheel_zoom", "pan"], 
             active_scroll="wheel_zoom", webgl=True)
fig.axis.visible = False
STAMEN_TONER_BACKGROUND = WMTSTileSource( url='http://tile.stamen.com/toner-background/{Z}/{X}/{Y}.png',
attribution=(
'Map tiles by <a href="http://stamen.com">Stamen Design</a>, '
'under <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>.'
'Data by <a href="http://openstreetmap.org">OpenStreetMap</a>, '
'under <a href="http://www.openstreetmap.org/copyright">ODbL</a>' ) )
fig.add_tile(STAMEN_TONER_BACKGROUND)

state_xs, state_ys = [], []
state_xs, state_ys = convert([us_states[code]["lons"] for code in us_states], 
                             [us_states[code]["lats"] for code in us_states])

abrev = [ab for ab in us_states.keys()]
tot = [len(accidents[accidents['STATE'] == ab]) for ab in abrev]
per_speed = [len(accidents[(accidents['STATE'] == ab) & (accidents['SPEEDING'] != 0)])*100. / float(tot[i]) for i, ab in enumerate(abrev)]
per_drunk = [len(accidents[(accidents['STATE'] == ab) & (accidents['DRUNK_DR'] != 0)])*100. / float(tot[i]) for i, ab in enumerate(abrev)]

state_dic = {'x_vals':state_xs, 'y_vals':state_ys, 'state':abrev, 
             'total':tot, 'p_drunk':per_drunk, 'p_speed':per_speed}

state_borders = ColumnDataSource(state_dic)
state_borders.add("white", name='color')

states = fig.patches(xs='x_vals', ys='y_vals', source=state_borders, 
                     fill_color='color',alpha=0.35, line_color="black", 
                     line_alpha=0.5, line_width=1)

fig.add_tools(HoverTool(renderers=[states], 
                        tooltips=[('State', '@state'), ('Total Accidents', '@total'),
                                  ('Drunk %', '@p_drunk'), ('Speeding %', '@p_speed')]))

select = Select(title="Option:", value="None", options=["None", "Drunk", "Speeding"])

Reds = Reds9[::-1]
Greens = Greens9[::-1]

no_colors = ['#FFFFFF']*len(abrev)
drunk_colors = [Reds[i] for i in pd.qcut(per_drunk, len(Reds)).codes]
speeding_colors = [Greens[i] for i in pd.qcut(per_speed, len(Greens)).codes]

def update_color(attrname, old, new):
    if new == 'Speeding':
        state_borders.data["color"] = speeding_colors
    if new == 'Drunk':
        state_borders.data["color"] = drunk_colors
    if new == 'None':
        state_borders.data["color"] = no_colors
    return

select.on_change('value', update_color)
curdoc().add_root(column(fig, select))
